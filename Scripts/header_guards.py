import imp
import os
import sys 


def LoadHeaderGuardScript(morefem_script_dir):
    """Load as an imp module the header guards script present in MoReFEM library. 
    
    This directory is also added to sys.path to enable discovery of its own module dependancies.
    
    \param[in] morefem_script_dir Path to the Scripts directory within MoReFEM library.
    
    \return Imp module loaded.
    """
    if not os.path.exists(morefem_script_dir):
        raise Exception("Error: the given Scripts dir ({0}) doesn't exist)".format(morefem_script_dir))
 
    sys.path.append(morefem_script_dir)
                                             
    original_header_guards_script = os.path.join(morefem_script_dir,
                                                 "header_guards.py")
                                                 
    if not os.path.exists(original_header_guards_script):
        raise Exception("Error: no header_guards.py file found in the given Scripts dir ({0})".format(morefem_script_dir))               
    local_header_guards = imp.load_source('header_guards', original_header_guards_script)
    
    return local_header_guards



if __name__ == "__main__":
    morefem_script_dir = os.path.join(os.environ["HOME"], \
                                      "Codes",
                                      "MoReFEM",
                                      "CoreLibrary",
                                      "Scripts")
    
    local_header_guards = LoadHeaderGuardScript(morefem_script_dir)
    
    directory = os.path.join(os.environ["HOME"], 'Codes', 'MoReFEM', 'Models', 'HyperelasticityIncompressible', 'Sources')
    ignore_list = list()
    
    local_header_guards.HeaderGuards("MorefemForHyperelasticityIncompressible", directory, ignore_list)
    