/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Model.hpp"


namespace MoReFEM
{
    
    
    namespace HyperelasticityIncompressibleNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            
            {
                const DirichletBoundaryConditionManager& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);
                
                auto&& bc_list =
                {
                    bc_manager.GetDirichletBoundaryConditionPtr("clamped")
                };
                
                
                variational_formulation_ =
                    std::make_unique<VariationalFormulation>(morefem_data,
                                                             GetNonCstTimeManager(),
                                                             god_of_dof,
                                                             std::move(bc_list));
            }
            
            auto& variational_formulation = GetNonCstVariationalFormulation();
            
            variational_formulation.Init(morefem_data.GetInputData());
            
            const auto& monolithic_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            const auto& mpi = GetMpi();
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            
            variational_formulation.SolveNonLinear(monolithic_numbering_subset,
                                                   monolithic_numbering_subset,
                                                   __FILE__, __LINE__);

            variational_formulation.WriteSolution(GetTimeManager(), monolithic_numbering_subset);

            variational_formulation.PrepareDynamicRuns();
        }
        
        
        void Model::SupplInitializeStep()
        {
        }


        void Model::Forward()
        {
            VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            const auto& monolithic_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            variational_formulation.SolveNonLinear(monolithic_numbering_subset,
                                                   monolithic_numbering_subset,
                                                   __FILE__, __LINE__);
        }


        void Model::SupplFinalizeStep()
        {
            auto& variational_formulation = GetNonCstVariationalFormulation();
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            const auto& monolithic_numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            variational_formulation.WriteSolution(GetTimeManager(), monolithic_numbering_subset);
            variational_formulation.UpdateForNextTimeStep();
        }


        void Model::SupplFinalize()
        {
        }
            

    } // namespace HyperelasticityIncompressibleNS


} // namespace MoReFEM
