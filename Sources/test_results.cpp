#define BOOST_TEST_MODULE model_hyperelasticity_incompressible
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension);


    struct Fixture : public TestNS::FixtureNS::Environment
    {
        Fixture();
    };


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(seq_2d, Fixture)
{
    CommonTestCase("Seq", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, Fixture)
{
    CommonTestCase("Mpi4", "2D");
}


BOOST_FIXTURE_TEST_CASE(seq_3d, Fixture)
{
    CommonTestCase("Seq", "3D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d, Fixture)
{
    CommonTestCase("Mpi4", "3D");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        (root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        (output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

		std::string ref_dir_path = root_dir + "/Sources/ExpectedResults/" + dimension;
		std::string obtained_dir_path = output_dir + std::string("/") + seq_or_par
		+ std::string("/IncompressibleHyperelasticity/") + dimension + "/Rank_0";

		FilesystemNS::Directory ref_dir(ref_dir_path,
										FilesystemNS::behaviour::read,
										__FILE__, __LINE__);

		FilesystemNS::Directory obtained_dir(obtained_dir_path,
											 FilesystemNS::behaviour::read,
											 __FILE__, __LINE__);

		TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0; i < 6; ++i)
        {
            oconv.str("");
            oconv << "displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
        }


        for (auto i = 0; i < 6; ++i)
        {
            oconv.str("");
            oconv << "pressure." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__, 1.e-7);
        }
    }


    Fixture::Fixture()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
            decltype(auto) cli_args = boost::unit_test::framework::master_test_suite().argv;

            environment.SetEnvironmentVariable(std::make_pair("MODEL_ROOT_DIRECTORY",
                                                              cli_args[1]),
                                               __FILE__, __LINE__);

            first_call = false;
        }
    }


} // namespace anonymous
