/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_VARIATIONAL_FORMULATION_HXX_
# define MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_VARIATIONAL_FORMULATION_HXX_


namespace MoReFEM
{


    namespace HyperelasticityIncompressibleNS
    {


        inline Wrappers::Petsc::Snes::SNESFunction VariationalFormulation::ImplementSnesFunction() const
        {
            return &VariationalFormulation::Function;
        }


        inline Wrappers::Petsc::Snes::SNESJacobian VariationalFormulation::ImplementSnesJacobian() const
        {
            return &VariationalFormulation::Jacobian;
        }


        inline Wrappers::Petsc::Snes::SNESViewer VariationalFormulation::ImplementSnesViewer() const
        {
            return &VariationalFormulation::Viewer;
        }


        inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
        {
            return nullptr;
        }


        inline const GlobalVariationalOperatorNS::Mass&
        VariationalFormulation::GetMassOperator() const noexcept
        {
            assert(!(!mass_operator_));
            return *mass_operator_;
        }


        inline const typename VariationalFormulation::incompressible_second_piola_type&
        VariationalFormulation::GetIncompressibleSecondPiolaOperator() const noexcept
        {
            assert(!(!incompressible_stiffness_operator_));
            return *incompressible_stiffness_operator_;
        }


        inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>&
        VariationalFormulation::GetSurfacicForceOperator() const noexcept
        {
            assert(!(!surfacic_force_operator_));
            return *surfacic_force_operator_;
        }


        inline const GlobalVector& VariationalFormulation::GetVectorStateAtNewtonIteration() const noexcept
        {
            assert(!(!vector_state_at_newton_iteration_));
            return *vector_state_at_newton_iteration_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorStateAtNewtonIteration() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorStateAtNewtonIteration());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorVelocityAtNewtonIteration() const noexcept
        {
            assert(!(!vector_velocity_at_newton_iteration_));
            return *vector_velocity_at_newton_iteration_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorVelocityAtNewtonIteration() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorVelocityAtNewtonIteration());
        }


        inline const GlobalMatrix& VariationalFormulation::GetMatrixMassPerSquareTimeStep() const noexcept
        {
            assert(!(!matrix_mass_per_square_time_step_));
            return *matrix_mass_per_square_time_step_;
        }


        inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixMassPerSquareTimeStep() noexcept
        {
            return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentState() const noexcept
        {
            assert(!(!vector_current_state_));
            return *vector_current_state_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentState() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentState());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorCurrentVelocity() const noexcept
        {
            assert(!(!vector_current_velocity_));
            return *vector_current_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointState() const noexcept
        {
            assert(!(!vector_midpoint_state_));
            return *vector_midpoint_state_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointState() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointState());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorMidpointVelocity() const noexcept
        {
            assert(!(!vector_midpoint_velocity_));
            return *vector_midpoint_velocity_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointVelocity() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorMidpointVelocity());
        }


        inline const GlobalVector& VariationalFormulation::GetVectorTemporaryState() const noexcept
        {
            assert(!(!vector_temporary_state_));
            return *vector_temporary_state_;
        }


        inline GlobalVector& VariationalFormulation::GetNonCstVectorTemporaryState() noexcept
        {
            return const_cast<GlobalVector&>(GetVectorTemporaryState());
        }


        inline const Solid& VariationalFormulation::GetSolid() const noexcept
        {
            assert(!(!solid_));
            return *solid_;
        }


    } // namespace HyperelasticityIncompressibleNS


} // namespace MoReFEM


#endif // MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_VARIATIONAL_FORMULATION_HXX_
