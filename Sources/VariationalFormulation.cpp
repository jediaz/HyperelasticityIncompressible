/// \file
///
///
/// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
/// Copyright (c) Inria. All rights reserved.
///

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "VariationalFormulation.hpp"


namespace MoReFEM
{
    
    
    namespace HyperelasticityIncompressibleNS
    {
        

        VariationalFormulation::VariationalFormulation(const morefem_data_type& morefem_data,
                                                       TimeManager& time_manager,
                                                       const GodOfDof& god_of_dof,
                                                       DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list))
        {
            assert(time_manager.IsTimeStepConstant() && "Current instantiation relies on this assumption!");
        }
        
        
        void VariationalFormulation::AllocateMatricesAndVectors()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            parent::AllocateSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            parent::AllocateSystemVector(monolithic_numbering_subset);
            
            const auto& monolithic_system_matrix = GetSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            const auto& monolithic_system_rhs = GetSystemRhs(monolithic_numbering_subset);

            vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            vector_state_at_newton_iteration_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            
            vector_current_state_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            vector_current_velocity_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            vector_midpoint_state_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            vector_midpoint_velocity_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            vector_temporary_state_ = std::make_unique<GlobalVector>(monolithic_system_rhs);
            
            matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(monolithic_system_matrix);
        }
        
        
        void VariationalFormulation::SupplInit(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            
            decltype(auto) domain_full_mesh =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            
            solid_ = std::make_unique<Solid>(input_data,
                                             domain_full_mesh,
                                             felt_space_volume.GetQuadratureRulePerTopology());

            DefineStaticOperators(input_data);
        }

        
        void VariationalFormulation::DefineStaticOperators(const InputData& input_data)
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            const auto& felt_space_force = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::force));
            
            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            const auto& pressure_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::pressure));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            namespace IPL = Utilities::InputDataNS;
            
            hyperelastic_deviatoric_law_parent::Create(GetSolid());
            penalization_law_parent::Create(GetSolid());
        
            const std::array<Unknown::const_shared_ptr, 2> displacement_pressure { { displacement_ptr, pressure_ptr } };

            incompressible_stiffness_operator_ =
                std::make_unique<incompressible_second_piola_type>(felt_space_volume,
                                                                   displacement_pressure,
                                                                   displacement_pressure,
                                                                   GetSolid(),
                                                                   GetTimeManager(),
                                                                   hyperelastic_deviatoric_law_parent::GetHyperelasticLawPtr(),
                                                                   penalization_law_parent::GetPenalizationLawPtr(),
                                                                   nullptr,
                                                                   nullptr);


            decltype(auto) domain_force =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::force), __FILE__, __LINE__);
            
            using parameter_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>;
            
            force_parameter_ =
                InitParameterFromInputData<ParameterNS::Type::vector>::template Perform<parameter_type>("Surfacic force",
                                                                                                        domain_force,
                                                                                                        input_data);
            
            if (force_parameter_ != nullptr)
            {
                surfacic_force_operator_ = std::make_unique<GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>>(felt_space_force,
                                                                                                                                     displacement_ptr,
                                                                                                                                     *force_parameter_);
            }
        }
        
        PetscErrorCode VariationalFormulation::Function(SNES snes,
                                                        Vec petsc_vec_displacement_at_newton_iteration,
                                                        Vec a_residual,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(a_residual); // a_residual is not used as its content mirror the one of system_rhs_.
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            #ifndef NDEBUG
            
            const auto& god_of_dof = variational_formulation.GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            {
                std::string desc;
                
                namespace Petsc = Wrappers::Petsc;
                
                const bool is_same = Petsc::AreEqual(Petsc::Vector(a_residual, false),
                                                     variational_formulation.GetSystemRhs(monolithic_numbering_subset),
                                                     1.e-12,
                                                     desc,
                                                     __FILE__, __LINE__);
                
                if (!is_same)
                {
                    std::cerr << "Bug in SNES implementation: third argument of snes function implemention is expected "
                    "to be the residual, which is born in VariationalFormulationT by the attribute RHS(). It is not the case "
                    "here.\n" << desc << std::endl;
                    assert(false);
                }
            }
            #endif // NDEBUG
            
            variational_formulation.UpdateVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
            variational_formulation.ComputeResidual();
            
            return 0;
        }
        
        
        void VariationalFormulation::UpdateVectorsAndMatrices(const Vec& petsc_vec_displacement_at_newton_iteration)
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    UpdateStaticVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
                    break;
                case StaticOrDynamic::dynamic_:
                    UpdateDynamicVectorsAndMatrices(petsc_vec_displacement_at_newton_iteration);
                    break;
            }
            
            AssembleOperators();
        }
        
        
        void VariationalFormulation::UpdateStaticVectorsAndMatrices(const Vec& petsc_vec_state_at_newton_iteration)
        {
            UpdateStateAtNewtonIteration(petsc_vec_state_at_newton_iteration);
        }
        
        
        void VariationalFormulation::UpdateStateAtNewtonIteration(Vec petsc_vec_state_at_newton_iteration)
        {
            auto& state_at_newton_iteration = GetNonCstVectorStateAtNewtonIteration();
            state_at_newton_iteration.SetFromPetscVec(petsc_vec_state_at_newton_iteration, __FILE__, __LINE__);
            state_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
            
            GetNonCstVectorCurrentState().UpdateGhosts(__FILE__, __LINE__);
        }
        
        void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const Vec& petsc_vec_state_at_newton_iteration)
        {
            UpdateStateAtNewtonIteration(petsc_vec_state_at_newton_iteration);
            
            UpdateVelocityAtNewtonIteration();
            
            auto& midpoint_state = GetNonCstVectorMidpointState();
            
            midpoint_state.Copy(GetVectorCurrentState(), __FILE__, __LINE__);
            Wrappers::Petsc::AXPY(1., GetVectorStateAtNewtonIteration(), midpoint_state, __FILE__, __LINE__);
            
            midpoint_state.Scale(0.5, __FILE__, __LINE__);
            
            midpoint_state.UpdateGhosts(__FILE__, __LINE__);
            
            auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();
            
            midpoint_velocity.Copy(GetVectorStateAtNewtonIteration(), __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(-1., GetVectorCurrentState(), midpoint_velocity, __FILE__, __LINE__);
            
            midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
            
            midpoint_velocity.UpdateGhosts(__FILE__, __LINE__);
            
            GetNonCstVectorCurrentState().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::UpdateVelocityAtNewtonIteration()
        {
            auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            
            if (GetSnes().GetSnesIteration(__FILE__, __LINE__) == 0)
            {
                velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity(), __FILE__, __LINE__);
            }
            else
            {
                const auto& state_previous_time_iteration = GetVectorCurrentState();
                const auto& state_at_newton_iteration = GetVectorStateAtNewtonIteration();
                
                auto& temporary_state = GetNonCstVectorTemporaryState();
                
                temporary_state.Copy(state_at_newton_iteration, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(-1., state_previous_time_iteration, temporary_state, __FILE__, __LINE__);
                temporary_state.Scale(2. / GetTimeManager().GetTimeStep(), __FILE__, __LINE__);
                velocity_at_newton_iteration.Copy(temporary_state, __FILE__, __LINE__);
                Wrappers::Petsc::AXPY(- 1., velocity_previous_time_iteration, velocity_at_newton_iteration, __FILE__, __LINE__);
            }
            
            velocity_at_newton_iteration.UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::AssembleOperators()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    AssembleNewtonStaticOperators();
                    break;
                case StaticOrDynamic::dynamic_:
                    AssembleNewtonDynamicOperators();
                    break;
            }
        }

        
        void VariationalFormulation::AssembleNewtonStaticOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            auto& monolithic_system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            auto& monolithic_system_rhs = GetNonCstSystemRhs(monolithic_numbering_subset);
            
            monolithic_system_matrix.ZeroEntries(__FILE__, __LINE__);
            monolithic_system_rhs.ZeroEntries(__FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient mat(monolithic_system_matrix, 1.);
                GlobalVectorWithCoefficient vec(monolithic_system_rhs, 1.);
                
                const GlobalVector& state_vector = GetVectorStateAtNewtonIteration();

                decltype(auto) incompressible_operator =
                    GetIncompressibleSecondPiolaOperator();
                
                incompressible_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            state_vector);
            }
            
            {
                GlobalVectorWithCoefficient vec(monolithic_system_rhs, 1.);
                
                const double time = parent::GetTimeManager().GetTime();
                
                GetSurfacicForceOperator().Assemble(std::make_tuple(std::ref(vec)),
                                                    time);
            }

        }
        
        void VariationalFormulation::AssembleNewtonDynamicOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            auto& monolithic_system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            auto& monolithic_system_rhs = GetNonCstSystemRhs(monolithic_numbering_subset);
            
            monolithic_system_matrix.ZeroEntries(__FILE__, __LINE__);
            monolithic_system_rhs.ZeroEntries(__FILE__, __LINE__);
            
            {
                GlobalMatrixWithCoefficient mat(monolithic_system_matrix, 0.5);
                GlobalVectorWithCoefficient vec(monolithic_system_rhs, 1.);
                
                const GlobalVector& midpoint_state_vector = GetVectorMidpointState();

                decltype(auto) incompressible_operator =
                    GetIncompressibleSecondPiolaOperator();

                incompressible_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                                midpoint_state_vector);
            }
            
        }
        
        
        void VariationalFormulation::ComputeResidual()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    ComputeStaticResidual();
                    break;
                case StaticOrDynamic::dynamic_:
                    ComputeDynamicResidual();
                    break;
            }
        }
        
        
        void VariationalFormulation::ComputeStaticResidual()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(monolithic_numbering_subset,
                                                                                      monolithic_numbering_subset);
        }
        
        
        void VariationalFormulation::ComputeDynamicResidual()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));

            auto& rhs = GetNonCstSystemRhs(monolithic_numbering_subset);
            
            const auto& time_manager = GetTimeManager();
            const double time_step = time_manager.GetTimeStep();
            const auto& state_previous_time_iteration = GetVectorCurrentState();
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            auto& temporary_state = GetNonCstVectorTemporaryState();
            temporary_state.Copy(GetVectorStateAtNewtonIteration(), __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(-1.,
                                  state_previous_time_iteration,
                                  temporary_state,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::AXPY(- time_step,
                                  velocity_previous_time_iteration,
                                  temporary_state,
                                  __FILE__, __LINE__);
            
            Wrappers::Petsc::MatMultAdd(GetMatrixMassPerSquareTimeStep(),
                                        temporary_state,
                                        rhs,
                                        rhs,
                                        __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(monolithic_numbering_subset,
                                                                                      monolithic_numbering_subset);
        }
        
        
        PetscErrorCode VariationalFormulation::Jacobian(SNES snes,
                                                        Vec evaluation_state,
                                                        Mat jacobian,
                                                        Mat preconditioner,
                                                        void* context_as_void)
        {
            static_cast<void>(snes);
            static_cast<void>(jacobian);
            static_cast<void>(preconditioner);
            static_cast<void>(evaluation_state);
            
            // \a context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            
            VariationalFormulation* variational_formulation_ptr = static_cast<VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            auto& variational_formulation = *variational_formulation_ptr;
            
            variational_formulation.ComputeTangent();
            
            return 0;
        }
        
        void VariationalFormulation::ComputeTangent()
        {
            switch(GetTimeManager().GetStaticOrDynamic())
            {
                case StaticOrDynamic::static_:
                    ComputeStaticTangent();
                    break;
                case StaticOrDynamic::dynamic_:
                    ComputeDynamicTangent();
                    break;
            }
        }
        
        void VariationalFormulation::ComputeStaticTangent()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(monolithic_numbering_subset,
                                                                                         monolithic_numbering_subset);
        }
        
        
        void VariationalFormulation::ComputeDynamicTangent()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
             auto& monolithic_system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
            
            #ifndef NDEBUG
            AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), monolithic_system_matrix);
            #endif // NDEBUG
                
            Wrappers::Petsc::AXPY<NonZeroPattern::subset>(1.,
                                                          GetMatrixMassPerSquareTimeStep(),
                                                          monolithic_system_matrix, __FILE__, __LINE__);
            
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix>(monolithic_numbering_subset,
                                                                                         monolithic_numbering_subset);
        }
        
        
        PetscErrorCode VariationalFormulation::Viewer(SNES snes,
                                                      PetscInt its,
                                                      PetscReal norm,
                                                      void* context_as_void)
        {
            static_cast<void>(snes);
            assert(context_as_void);
            
            const VariationalFormulation* variational_formulation_ptr =
            static_cast<const VariationalFormulation*>(context_as_void);
            
            assert(!(!variational_formulation_ptr));
            
            const auto& variational_formulation = *variational_formulation_ptr;
            
            const auto& state = variational_formulation.GetVectorStateAtNewtonIteration();
            const auto& velocity = variational_formulation.GetVectorVelocityAtNewtonIteration();
            
            const PetscReal displacement_min = state.Min(__FILE__, __LINE__).second;
            const PetscReal displacement_max = state.Max(__FILE__, __LINE__).second;
            
            const PetscReal velocity_min = velocity.Min(__FILE__, __LINE__).second;
            const PetscReal velocity_max = velocity.Max(__FILE__, __LINE__).second;
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("%3D Residual norm: %1.12e Ymin: %8.6e Ymax: %8.6e Ypmin: %8.6e Ypmax: %8.6e\n",
                                                          variational_formulation.GetMpi(),
                                                          __FILE__, __LINE__,
                                                          its + 1,
                                                          norm,
                                                          displacement_min,
                                                          displacement_max,
                                                          velocity_min,
                                                          velocity_max);
            
            return 0;
        }


        void VariationalFormulation::PrepareDynamicRuns()
        {
            DefineDynamicOperators();
            
            AssembleDynamicOperators();
            
            UpdateForNextTimeStep();
        }
        
        
        void VariationalFormulation::DefineDynamicOperators()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& felt_space_volume = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::volume));
            
            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacement));
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume,
                                                         displacement_ptr,
                                                         displacement_ptr);
        }
        
        
        void VariationalFormulation::AssembleDynamicOperators()
        {
            const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();
            
            const double time_step = GetTimeManager().GetTimeStep();
            
            const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);
            
            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(),
                                               mass_coefficient);
            
            GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }
        
        
        void VariationalFormulation::UpdateForNextTimeStep()
        {
            UpdateStateBetweenTimeStep();
            
            UpdateVelocityBetweenTimeStep();
            
            //ComputeGuessForNextTimeStep();
        }
        
        
        void VariationalFormulation::UpdateStateBetweenTimeStep()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            GetNonCstVectorCurrentState().Copy(GetSystemSolution(monolithic_numbering_subset),
                                               __FILE__, __LINE__);
            
            GetNonCstVectorCurrentState().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::UpdateVelocityBetweenTimeStep()
        {
            GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration(),
                                                  __FILE__, __LINE__);
            
            GetNonCstVectorCurrentVelocity().UpdateGhosts(__FILE__, __LINE__);
        }
        
        
        void VariationalFormulation::ComputeGuessForNextTimeStep()
        {
            const auto& god_of_dof = GetGodOfDof();
            const auto& monolithic_numbering_subset =
                god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            auto& system_solution = GetNonCstSystemSolution(monolithic_numbering_subset);
            const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
            
            Wrappers::Petsc::AXPY(GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution, __FILE__, __LINE__);
            
            system_solution.UpdateGhosts(__FILE__, __LINE__);
        }
        
        
    } // namespace HyperelasticityIncompressibleNS


} // namespace MoReFEM
