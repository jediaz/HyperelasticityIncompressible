v19.52:

- Feature #1: Introduce CI.

v19.47:

- Update to the new MoReFEM API

v19.27.2:

- Update to the new MoReFEM API and adapt some expected results files accordingly.
- Also adapt Lua files.
- Detail the Solid parameters in the input data file.

v19.16:

- Update to the new MoReFEM API (especially tests now handled with Boost.Test).
- Fix the README.


v18.47:

- MoReFEM Support #1374: Replace MOREFEM_ROOT by more accurate MODEL_ROOT_DIRECTORY.
- MoReFEM Support #1373: Update the model to the v18.47 MoReFEM API.
- **MoReFEM Support #1371**: Propagate this ticket from MoReFEM library that rename the InputParameterList InputData.


v18.32:

- MoReFEM Feature #1321: Add integration test.


v18.30:

- MoReFEM Support #1309: Update the Lua files to the simplified new interface for parameters.


v18.27:

- Make it compliant with MoReFEM v18.27.


v18.12.2

- MoReFEM Support #1255: Update model to work with MoReFEM v18.12.2. SCons build has been replaced by CMake one.